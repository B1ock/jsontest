package com.hladush.writer;

public interface Writer  {
    void write(final Object object);
}
