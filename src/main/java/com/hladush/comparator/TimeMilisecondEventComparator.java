package com.hladush.comparator;

import com.hladush.model.LogEvent;

import java.util.Comparator;

public class TimeMilisecondEventComparator implements Comparator<LogEvent> {

    @Override
    public int compare(LogEvent event1, LogEvent event2) {
        return Integer.compare(event1.getTimeInMillisecond(), event2.getTimeInMillisecond());
    }
}


